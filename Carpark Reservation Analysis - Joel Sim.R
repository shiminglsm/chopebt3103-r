#################################################
# BT3103 (G09) - Chope-A-Lot
# Title: Carpark Reservation Analysis
# Name: Sim Teng Kuan Joel
# Matric No: A0162624J

#################################################
#### Data Preparation ####

library(jsonlite)
library(data.table)
library(httr)
library(forecast)
library(TTR)
library(shiny)
library(ggplot2)
library(ggrepel)

# Get the json file from firebase
data <- GET("https://chope-a-lot.firebaseio.com/.json?=auth=cr2cJdLGbMTRMjSKluvdZqZ5O5SsqScOY2PJ9uY8")
raise <- content(data, as="text")
read_data = fromJSON(raise)

# Put the data into frame and display in tabular form
timedata = data.frame()
for (i in 1:length(read_data$Time)){
  timedata = rbind(timedata, as.data.frame(read_data$Time[[i]], stringsAsFactors = FALSE))
}

# Convert the dates into Date format
for (i in 1: length(timedata$DateCheckedIn)){
  timedata$DateCheckedIn = as.Date(timedata$DateCheckedIn)
}

# To get the hours from the time checked in and insert into new column, eg: 09:01 & 09:59 = 09 hours
for(i in 1: length(timedata$TimeCheckedIn)){
  timedata$Hour[i] <- substring(timedata$TimeCheckedIn[i], 1, 2)
}

# Get a list of unique carpark name to populate the dropdown list for user input
carparkName = unique(timedata$CarparkName)

# Get a list of unique dates from the data
uniqueDates = unique(timedata$DateCheckedIn)

# A list of days to populate the dropdown list for user input
days = c("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")

# Store the list of time interval 
xrange = c("09","10","11","12","13","14","15","16","17","18","19","20","21")

# Get the data carpark name, day, hour and the number of reservations
tmp = data.table(timedata)[,.(.N),by=.(CarparkName, DateCheckedIn, Hour)]

# To get 0 for the frequency table (for line graph plot)
carpark = c(carparkName)
udates = c(uniqueDates)
tmp2 = expand.grid(carpark, udates, xrange)
colnames(tmp2) <- c("CarparkName", "DateCheckedIn", "Hour")
tmp3 = merge(tmp2, tmp, by=c("CarparkName","DateCheckedIn", "Hour"), all.x=T)
tmp3[is.na(tmp3)] <- 0

# Reference the day to the dates in the table
for(i in 1:length(tmp3$DateCheckedIn)){
  tmp3$Day[i] <- (weekdays(tmp3$DateCheckedIn[i]))
}

#################################################
#### User Interface ####

ui <- fluidPage(
  titlePanel("Carpark Reservation Analysis"),

    tabsetPanel(
      tabPanel(
        "Peak Period Analysis", 
        
        sidebarLayout(
          sidebarPanel(
            # Display the dropdown list from the unique carpark name in alphabetical order
            selectInput(inputId = "carpark", 
                      label = "Select a carpark:", 
                      choices = sort(carparkName)),
            
            # Allow user to input a start and end date range 
            dateRangeInput(inputId = "s_date",
                         label = "View the graph between",
                         start = "2017-01-01",
                         end = "2017-11-20"),
            
            # Display dropdown list of days 
            selectInput(inputId = "day", 
                      label = "Select a day:", 
                      choices = days),
            
            # Allow user to download the data for the carpark, date range and days that he selected
            helpText("Click on the download button to download the dataset observations"),
            downloadButton("downloadData", "Download Data")
            ),
          
            mainPanel(
              plotOutput("peakCarparkDay")
            )
          ) # end of sidebarLayout
        ), # end of tabPanel (Peak Period Analysis)
      
        tabPanel(
          "Simple Moving Average", 
          
          sidebarLayout(
            sidebarPanel(
              # Display the dropdown list from the unique carpark name in alphabetical order
              selectInput(inputId = "macarpark", 
                          label = "Select a carpark:", 
                          choices = sort(carparkName))
            ),
            
            mainPanel(
              plotOutput("movingAvg"))
          )
        ) # end of tabPanelC (Simple Moving Average)
      ) # end of tabsetPanel 
    )

#### Reactive Output ####

server <- function(input, output) {
  
  # This reactive code will be used for the reactive line graph
  mydata <- reactive({
    
    # Filtering the data according to user's input, and count the frequency grouped according to carpark name, day, date checked in, hour
    react.data <- data.table(tmp3[which(as.character(tmp3$CarparkName) == as.character(input$carpark) & 
                                (as.character(tmp3$Day) == as.character(input$day)) &
                                (tmp3$DateCheckedIn >= as.Date(input$s_date[1]) & 
                                  tmp3$DateCheckedIn <= as.Date(input$s_date[2]))),])[by=.(CarparkName, Day, DateCheckedIn, Hour)]

    # To find the average of N, grouped according to Hour
    agg.data <- aggregate(react.data$N, by = list(react.data$Hour), mean)
  })
  
  # Reactive line graph 
  output$peakCarparkDay <- renderPlot({
    ggplot(mydata(), aes(x=Group.1,y=x,group=1)) + geom_line() + geom_text_repel(aes(label=x)) +
      labs(y="Average Number of Reservations", x="Hours", title="Average Number of Reservations per Carpark") +
      theme_classic() + scale_x_discrete(limit = xrange)
  })
  
  # This reactive code will be used for simple moving average
  mydata2 <- reactive({
    
    # Get the data which the carpark name is what the user selected
    tmp4 <- subset(tmp3, tmp3$CarparkName == input$macarpark) 
    
    # Removing CarparkName, Day, DateCheckedIn, Hour
    tmp5 <- tmp4[,-c(1)]
    tmp6 <- tmp5[,-c(1)]
    tmp7 <- tmp6[,-c(1)]
    tmp8 <- tmp7[,-c(2)]
    
    # Store data into time series object
    # Freq = 13 since the time range is from 9am to 9pm (13 hours)
    myts <- ts(tmp8, freq = 13)
    
    # Order of 2 is chosen because it has the smallest MSE value for all carparks
    movavg.forecast <- ma(myts, order = 2)
    movavg.TTR <- SMA(myts, 2)
  })
  
  # Reactive Moving Average Plot
  output$movingAvg <- renderPlot({
    plot.ts(mydata2())
  })
  
  # This reactive code will be used for downloading the dataset. 
  # The code is similar to mydata(), just that this is not aggregated by average.
  mydata3 <- reactive({
    download.data <- data.table(tmp3[which(as.character(tmp3$CarparkName) == as.character(input$carpark) & 
                                             (as.character(tmp3$Day) == as.character(input$day)) &
                                             (tmp3$DateCheckedIn >= as.Date(input$s_date[1]) & 
                                                tmp3$DateCheckedIn <= as.Date(input$s_date[2]))),])[by=.(CarparkName, Day, DateCheckedIn, Hour)]
  })
  
  # Downloading of data as csv
  output$downloadData <- downloadHandler(
    filename = function(){
      paste("timedata-", Sys.Date(), ".csv", sep = "")
    },
    
    content = function(file){
      write.csv(mydata3(), file, row.names = FALSE)
    }
  )
  
}

shinyApp(ui = ui, server = server)
